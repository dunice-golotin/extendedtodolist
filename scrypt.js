var Storage;
if(window.sessionStorage.getItem('Storage')){
    Storage=JSON.parse(window.sessionStorage.getItem('Storage'));
}
else {
    Storage=[];
}
var pagination;

if(window.sessionStorage.getItem('pagination')){
    pagination=JSON.parse(window.sessionStorage.getItem('pagination'));
    $('#pagin')[0].value=pagination;
}
else{
    pagination=$('#pagin')[0].value;
    window.sessionStorage.setItem('pagination',JSON.stringify(pagination));
}
var startPosition=0;

function paginationButtonCreate(){
    var numberOfButton=Math.ceil(Storage.length/pagination);
    $('#pagindiv').remove();
    if(numberOfButton==1){
        return;
    }
    var divPagin=$('<div/>',{class:'pagination', id: 'pagindiv'});
    for(var i=0; i<numberOfButton; i++){
        divPagin.append($('<input>',{type:'submit',value:`${i+1}`, class:'paginbutton',id:`paginbutton${i}`}));
    }
    $('table').append(divPagin);
}

$('#generalsubmit').click(function(){
    var title = $('#title').val();
    var author = $('#author').val();
    if(title == '' || author==''){
        alert("Void string");
        return;
    }
    var locStorg={
        "id": +(new Date()),
        "author": $('#author').val(),
        "title": $('#title').val(),
        "done": false,
        "updated":+(new Date()),
    }
    Storage.push(locStorg);
    window.sessionStorage.setItem('Storage',JSON.stringify(Storage));
})

createHeader();

function createHeader(){
    var thead=$('<thead/>',{id:'theadID'})
    var tableHeader=$('<tr/>',{id:'tablheader'})
    $.each(Storage[0],function(index,value){
        if(index=='id'){
            return;
        }
        
        var tableHCell=$('<th/>',{id:'header'+index,class:'headteble',}).html(`<div>${index}</div>`)
        var sortButtonUp=$('<input>',{type: 'submit',id:'sortup'+index,class:'sortbutton',value:'⇧'});
        var sortButtonDown=$('<input>',{type: 'submit',id:'sortdn'+index,class:'sortbutton',value:'⇩'});
        tableHCell.append(sortButtonUp).append(sortButtonDown);
        tableHeader.append(tableHCell);
    })
    thead.append(tableHeader);
    $('#tableStorage').append(thead);
}
//create table for display data
drawTable();
function drawTable(){
    $('.datarow').remove();

    while(startPosition*pagination>=Storage.length){
        startPosition--;
    }

    $.each(Storage,function(i,lineStorage){
        if(i<startPosition*(+pagination) || i>(startPosition+1)*(+pagination)-1){
            return;
        }
        var tableCell=$('<tr/>',{id:i+'tr',class:'datarow'});
        $('#tableStorage').append(tableCell);
        $.each(lineStorage,function(index,value){
            switch(index){
                case "title":tableCell.append(`<td id="${i+index}"><div id="div${i+index}">${value}</div></td>`);
                break;
                case "author":tableCell.append(`<td id="${i+index}"><div id="div${i+index}">${value}</div></td>`);
                break;
                case"done": {
                    var tableData=$('<td/>',{id:i+index});
                    if(!value==false){
                        tableData.append($(`<input type="checkbox" id="ch${i+index}">`).prop("checked", true));
                    }
                    else{
                        tableData.append($(`<input type="checkbox" id="ch${i+index}">`).prop("checked", false));
                    }
                    tableCell.append(tableData);
                }
                break;
                case "updated":tableCell.append(`<td id="${i+index}"><div id="div${i+index}">${dataUpdated(value)}</div></td>`);
                break;
            }
        })
        var tableData=$('<td/>',{id: i+'delete', class:'deletcore'});
        tableData.append($(`<input>`,{type:"submit", id: i+'del', class: "del", value: "delete"}));
        tableCell.append(tableData);
    })

    paginationButtonCreate();
}

function dataUpdated(value) {
    var D=new Date(value);
    return `${D.getDate()}-${D.getMonth()}-${D.getFullYear()} ${D.getHours()}:${D.getMinutes()}`;
}

//create event for table
$('#tableStorage').on('dblclick',function(event){
    if(event.target.id.substr(4)=='title' || event.target.id.substr(4)=='author'){
        var datavalue=$('#'+event.target.id).text();
        var parant=$('#'+event.target.id).parent();
        var textArea=$('<textarea>',{id:event.target.id});
        textArea.append(datavalue);
        event.target.remove();
        parant.append(textArea);
        textArea.focus();
        textArea.on('blur',function(event){
            textArea.off('blur');
            if(Storage[+textArea[0].id[3]][textArea[0].id.substr(4)]==textArea.val() || textArea.val().trim()==''){
                parant.append(`<div id="${textArea[0].id}">${Storage[+textArea[0].id[3]][textArea[0].id.substr(4)]}</div>`);
                textArea.remove();
            }
            else{
                Storage[+textArea[0].id[3]][textArea[0].id.substr(4)]=textArea.val().trim();
                Storage[+textArea[0].id[3]]["updated"]=+(new Date())
                parant.append(`<div id="${textArea[0].id}">${textArea.val().trim()}</div>`);
                textArea.remove();
                $(`#div${textArea[0].id[3]}updated`).replaceWith(`<div id="div${textArea[0].id[3]}updated">${dataUpdated(+(new Date()))}</div>`);
                window.sessionStorage.setItem('Storage',JSON.stringify(Storage));
            }
        });
    }
})

$('#tableStorage').on('change',function(event){
    if(event.target.id.substr(3)=='done'){
        Storage[event.target.id[2]]['done']=!Storage[event.target.id[2]]['done'];
        Storage[event.target.id[2]]['updated']=+(new Date());
        $('#div'+event.target.id[2]+'updated').html(dataUpdated(Storage[event.target.id[2]]['updated']));
        window.sessionStorage.setItem('Storage',JSON.stringify(Storage));
    }    
})
//delete
$('#tableStorage').on('click','.del',function(event){
    var index_delete=event.target.id[0];
    ($('#'+event.target.id).parents('tr')).remove();
    Storage.splice(index_delete,1);
    window.sessionStorage.setItem('Storage',JSON.stringify(Storage));
    paginationButtonCreate();
    drawTable();
})

//сортировка
function sortUpdate(a,b){
    return a.updated-b.updated
}

function compareTitle(a,b){
    var sumA=0;
    var sumB=0;
    var long= (a.title.length<b.title.length) ? a.title.length: b.title.length;
    for(var i=0; i<long; i++){
        sumA=sumA*10+a.title.charCodeAt(i);
        sumB=sumB*10+b.title.charCodeAt(i);
    }
    if(sumA*10+a.title.length>sumB*10+b.title.length){
        return 1;
    }
    else{
        return -1;
    }  
}

function compareAuthor(a,b){
    var sumA=0;
    var sumB=0;
    var long= (a.author.length<b.author.length) ? a.author.length: b.author.length;
    for(var i=0; i<long; i++){
        sumA=sumA*10+a.author.charCodeAt(i);
        sumB=sumB*10+b.author.charCodeAt(i);
    }
    if(sumA*10+a.author.length>sumB*10+b.author.length){
        return 1;
    }
    else {
        return -1;
    }  
}

function compareStatus(a,b){
    if(b.done>a.done){
        return -1;
    }
    else{
        return 1;
    }
}

$('#tableStorage').on('click','.sortbutton',function(event){
    switch(event.target.id.substr(6)){
        case 'title': Storage.sort(compareTitle);
        break;
        case 'author': Storage.sort(compareAuthor);
        break;
        case 'done': Storage.sort(compareStatus);
        break;
        case 'updated': Storage.sort(sortUpdate);
        break;
    }
    if(event.target.id.substr(4,2)=='dn'){
        Storage.reverse();
    }
    window.sessionStorage.setItem('Storage',JSON.stringify(Storage));
    drawTable();    
})
//change pagin value
$('#pagin').on('change',function(event){
    pagination=event.target.value;
    window.sessionStorage.setItem('pagination',JSON.stringify(pagination));
    drawTable();
    paginationButtonCreate();
})
 $('body').on('click','.paginbutton',function(event){
    startPosition=+(event.target.value)-1;
    drawTable()
})   